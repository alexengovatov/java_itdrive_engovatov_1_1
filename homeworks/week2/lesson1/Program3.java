import java.util.Scanner; 

class Program3 {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int a = scanner.nextInt();
		int b = scanner.nextInt();
		int result = sum(a, b);
		System.out.println(result);

	}

	public static int sum(int a, int b) {
		if (a > b) {
			int temp = a;
			a = b;
			b = temp;
		}
		int sum = 0;
		for (int i = a; i <= b; i++)
			sum += i;
		return sum;
	}
}