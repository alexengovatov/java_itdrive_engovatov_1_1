import java.util.Scanner;

class Program2 {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int number = scanner.nextInt();
		int result = mirror(number);
		System.out.println(result);
	}

	public static int mirror (int number) {
		int k = 1;
		int result = 0;
		while (number > 0) {
			result = k * result + (number % 10);
			k = 10;
			number /= 10; 
		}
		return result;
	}
}