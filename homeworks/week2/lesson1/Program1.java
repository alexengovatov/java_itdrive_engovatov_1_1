import java.util.Scanner;

class Program1 {
	public static void main(String[] args) {
	
	Scanner scanner = new Scanner(System.in);
	int number = scanner.nextInt();
	int result = sumOfNumbers(number);
	System.out.println(result);

	}

	public static int sumOfNumbers (int number) {
		int sum = 0;
		while (number > 0) {
			sum += (number % 10);
			number /= 10;
		}
		return sum;
	}

}