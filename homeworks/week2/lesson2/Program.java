import java.util.Scanner;
import java.util.Formatter;

class Program {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);

		double a = scanner.nextDouble();
		double b = scanner.nextDouble();
		int n = scanner.nextInt();

		System.out.format("%12s%10d%c%10f%n", "Simpson", n, ' ', Simpson(a, b, n));
	    System.out.format("%12s%10d%c%10f", "rectangle", n, ' ', Rectangle(a, b, n));
		
	}

	public static double f(double x) {
		return x * x;
	}

	public static double Rectangle (double a, double b, int n) {
		double h = (b - a) / n;	

		double result = 0;

		for (double x = a; x <= b; x = x + h) {
			double currentRectangle = f(x) * h;
			result = result + currentRectangle;
		}

		return result;
	}

	public static double Simpson (double a, double b, int n) {

		double h = (b - a) / n;

		double result = 0;

		for (double x = a + h; x <= b - h; x = x + 2 * h) {
			result = result + (f(x - h) + 4 * f(x) + f(x + h));
		}

		result = result * h / 3;

		return result;
	}
}