import java.util.Scanner;

class Program1 {
	public static void main(String[] args) {
		int [] a = new int [10];
		Scanner scanner = new Scanner(System.in);
		for (int i = 0; i < a.length; i++) {
			a[i] = scanner.nextInt();
		}
		sort(a);
		for (int i = 0; i < a.length; i++) {
			System.out.print(a[i] + " ");
		}
	}
	public static void sort(int [] a) {
		for (int i = 0; i < a.length; i++) {
			for (int j = i; j < a.length; j++){
				if (a[j] < a[i]) {
					int temp = a[i];
					a[i] = a[j];
					a[j] = temp;
				}
			}
		}
	}
}