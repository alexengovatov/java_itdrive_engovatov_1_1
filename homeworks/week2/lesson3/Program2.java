import java.util.Scanner;

class Program2 {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int [] a = new int[10];
		for (int i = 0; i < a.length; i++)
			a[i] = scanner.nextInt();
			sort(a);
			int numberForSearch = scanner.nextInt();
			binarySearch(a, numberForSearch);
	}

	public static void sort (int [] a) {
		for(int i = 0; i < a.length; i++) {
			for (int j = i; j < a.length; j++) {
				if (a[j] < a[i]) {
					int temp = a[i];
					a[i] = a[j];
					a[j] = temp;
				}	
			}
		}
		for (int i = 0; i < a.length; i++) {
			System.out.println(a[i]);
		}
	}

	public static void binarySearch (int [] a, int numberForSearch) {
		
		int left = 0;
		int right = a.length - 1;
		int middle;

		boolean find = false;	
		while (left <= right) {
			middle = (left + right) / 2;
			if (a[middle] < numberForSearch) {
				left = middle + 1;
			}
			else if (a[middle] > numberForSearch) {
				right = middle - 1;
			}
			else {
				System.out.println("Exists");
				find = true;
				break;
			}
		}

		if (find == false) {
			System.out.println("Not Exists");
		}
	}
}