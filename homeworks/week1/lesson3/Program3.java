import java.util.Scanner;

class Program3 {
	public static void main(String[] args) {
	Scanner scanner = new Scanner(System.in);
	long binaryDigit = 0;
	int number = scanner.nextInt();
	int k = 1;	
		while (number > 0) {
			binaryDigit = k * (number % 2) + binaryDigit; 
			number /= 2;
			k *= 10;

		}
	System.out.println(binaryDigit);	
	}
}