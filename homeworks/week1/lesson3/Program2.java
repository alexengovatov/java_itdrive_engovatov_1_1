import java.util.Scanner;
class Program2 {
	public static void main(String[] args) {
	Scanner scanner = new Scanner(System.in);
	int digit = scanner.nextInt();
	int mirror = 0;
	while (digit > 0) {
		mirror = mirror * 10 + (digit % 10);
		digit /= 10;
		}
	System.out.println(mirror);
	}
}