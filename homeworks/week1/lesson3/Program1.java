import java.util.Scanner;

class Program1{
public static void main(String[] args) {
	Scanner scanner = new Scanner(System.in);
	int digit = scanner.nextInt();
	int sum = 0;
	while (digit > 0) {
		sum += digit % 10;
		digit /= 10;
		}
	System.out.println(sum);
	}
}