import java.util.Scanner;

class Program {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		long mult = 1;
		int k = scanner.nextInt();
		while (k != -1) {
			boolean q = isEven(k);
			if (q) 
				mult *= k;
			k = scanner.nextInt();

		}
		System.out.println(mult);
	}

	public static boolean isEven(int a) {
		int z = 0;
		while (a > 0) {
			z = z + (a % 10);
			a /= 10;
		}
		if (z % 2 == 0)
			return true;
		else return false;
	}
}