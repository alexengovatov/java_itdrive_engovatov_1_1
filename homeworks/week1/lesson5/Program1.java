import java.util.Scanner;

class Program1{
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int length = scanner.nextInt();
		int [] a = new int[length];

		for (int i = 0; i < length; i++) {
			a[i] = scanner.nextInt();
		}

		int min = a[0];
		int max = a[0];
		int indexMin = 0;
		int indexMax = 0;

		for (int i = 0; i < length; i++) {
			if (a[i] < min) {
				min = a[i];
				indexMin = i;
			}

			if (a[i] > max) {
				max = a[i];
				indexMax = i;
			}
		}

		min = a [indexMin];
		a[indexMin] = a[indexMax];
		a[indexMax] = min;

		for (int i = 0; i < length; i++) {
			System.out.print(a[i] + " ");
		}
	}
}