import java.util.Scanner;

class Program2 {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int length = scanner.nextInt();
		int sum = 0;
		double avg = 0.0;
		int [] a = new int [length];

		for (int i = 0; i < length; i++) {
			a[i] = scanner.nextInt();
			sum += a[i];
		}

		avg = sum * 1.0 / length;
		System.out.println(avg);
	}
}