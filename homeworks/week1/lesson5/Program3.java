import java.util.Scanner;

class Program3 {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int length = scanner.nextInt();
		int temp = 0;
		int [] a = new int [length];

		for (int i = 0; i < length; i++) {
			a[i] = scanner.nextInt();
		}

		for (int i = 0; i <= length/2; i++) {
			temp = a[i];
			a[i] = a[length - i - 1];
			a[length - i - 1] = temp;
		}

		for (int i = 0; i < length; i++) {
			System.out.print(a[i] + " ");
		}
	}
}